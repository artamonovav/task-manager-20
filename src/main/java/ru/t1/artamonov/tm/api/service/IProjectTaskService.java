package ru.t1.artamonov.tm.api.service;

public interface IProjectTaskService {

    void bindTaskToProject(final String userId, final String projectId, final String taskId);

    void removeProjectById(final String userId, final String projectId);

    void unbindTaskFromProject(final String userId, final String projectId, final String taskId);

}
