package ru.t1.artamonov.tm.api.service;

import ru.t1.artamonov.tm.enumerated.Role;
import ru.t1.artamonov.tm.model.User;

public interface IAuthService {

    void checkRoles(Role[] roles);

    String getUserId();

    User getUser();

    boolean isAuth();

    void login(String login, String password);

    void logout();

    User registry(String login, String password, String email);

}
